<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class,'index'])->name('home');
Route::get('/{board}/', [\App\Http\Controllers\BoardController::class,'index'])->name('board');
Route::post('/{board}/', [\App\Http\Controllers\ThreadController::class,'create'])->name('thread.create');
Route::get('/{board}/{thread}', [\App\Http\Controllers\ThreadController::class,'index'])->name('thread');
Route::post('/{board}/{thread}', [\App\Http\Controllers\PostController::class,'create'])->name('post.create');
