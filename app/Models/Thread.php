<?php

namespace App\Models;

use App\Services\UserKeyGeneratorService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Thread
 *
 * @property int $id
 * @property string $op_cookie
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Thread newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Thread newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Thread query()
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereOpCookie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereUpdatedAt($value)
 * @property string $op_key
 * @property int $board_id
 * @property-read \App\Models\Board|null $board
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Post> $posts
 * @property-read int|null $posts_count
 * @property-read \App\Models\Post|null $threadPost
 * @method static \Database\Factories\ThreadFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereBoardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Thread whereOpKey($value)
 * @mixin \Eloquent
 */
class Thread extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function board(): BelongsTo
    {
        return $this->belongsTo(Board::class);
    }
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    public function threadPost(): HasOne
    {
        return $this->hasOne(Post::class)->where('thread_post', true);
    }
    public function recentPosts($limit = 3)
    {
        $posts = $this->hasMany(Post::class)
            ->orderBy('id', 'desc')
            ->take($limit)
            ->get();
        return $posts->sortBy('id');
    }

    public function check(){
        $gen = app(UserKeyGeneratorService::class);
        return $gen->check($this->op_key);
    }



}
