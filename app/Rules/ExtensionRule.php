<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ExtensionRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $extension = ['mp4','webm','webp','jpg','jpeg','png'];
        foreach ($value as $file){
            //dd($file->getClientOriginalExtension());
            if (!in_array($file->getClientOriginalExtension(),$extension)){
                $fail('File must have valid extension');
            }
        }

    }
}
