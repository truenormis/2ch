<?php

namespace App\Services\string;

use App\Models\Post;

class StringBuilder
{

    public function __construct(private string $string)
    {
    }

    public function findLinks(): StringBuilder
    {
        $pattern = '~[a-z]+://\S+~';
        if ($num_found = preg_match_all($pattern, $this->string, $out)) {
            foreach ($out[0] as $link) {
                $link_with_tag = "[$link]($link)";
                $this->string = str_replace($link, $link_with_tag, $this->string);
            }

        }
        return $this;
    }

    public function findPostReply(): StringBuilder
    {
        $pattern = '/>>(\d+)/';


        $this->string = preg_replace_callback($pattern, function ($matches) {
            $number = $matches[1];

            $post = Post::find($number);

            if ($post) {
                $link = route('thread', ['board' => $post->thread->board->board, 'thread' => $post->thread]) . "#$number";
                return "[>>$number]($link)";
            } else {
                return $matches[0];
            }
        }, $this->string);

        return $this;
    }

    public function tagsToHtml(): StringBuilder
    {
//        $tagsToReplace = ['[b]', '[/b]', '[i]', '[/i]', '[u]', '[/u]', '[o]', '[/o]', '[spoiler]', '[/spoiler]', '[s]', '[/s]'];
//        $htmlTags = ['<b>', '</b>', '<i>', '</i>', '<u>', '</u>', '<strike>', '</strike>', '', '</spoiler>', '<s>', '</s>'];
//
//        foreach ($tagsToReplace as $tag) {
//            $openingTagCount = substr_count($this->string, $tag);
//            $closingTagCount = substr_count($this->string, str_replace('[', '[/', $tag));
//            if ($openingTagCount !== $closingTagCount) {
//                $this->string = str_replace($tag, '', $this->string);
//            }
//        }




        $tags = [
            'b',
            'i',
            'u',
            's',
            'spoiler' => [
                '<spoiler class="transition duration-200 ease-in-out  bg-gray-900 text-gray-900 rounded hover:bg-transparent">',
                '</spoiler>'
            ]
        ];

        $this->string = $this->replace_tags($this->string,$tags);
        return $this;
    }
    private function replace_tags($text, $tags)
    {
        foreach ($tags as $tag => $html_tags) {
            if (is_numeric($tag)) {
                $start_tag = '[' . $html_tags . ']';
                $end_tag = '[/' . $html_tags . ']';
                $html_start_tag = '<' . $html_tags . '>';
                $html_end_tag = '</' . $html_tags . '>';
            } else {
                $start_tag = '[' . $tag . ']';
                $end_tag = '[/' . $tag . ']';
                $html_start_tag = $html_tags[0];
                $html_end_tag = $html_tags[1];
            }

            $start_pos = strpos($text, $start_tag);
            $end_pos = strpos($text, $end_tag);

            if ($start_pos !== false && $end_pos !== false && $start_pos < $end_pos) {
                $text = substr_replace($text, $html_start_tag, $start_pos, strlen($start_tag));
                $end_pos = $end_pos - strlen($start_tag) + strlen($html_start_tag);
                $text = substr_replace($text, $html_end_tag, $end_pos, strlen($end_tag));
            }
        }

        return $text;
    }
    public function mdLinkToHtml(): StringBuilder
    {
        $pattern = "/\[([^\]]+)\]\(([^)]+)\)/";

        $replacement = '<a href="$2">$1</a>';
        $this->string = preg_replace($pattern, $replacement, $this->string);
        return $this;
    }


    public function build(): string
    {
        return $this->string;
    }

    public function addBr(): StringBuilder
    {
        $this->string = nl2br($this->string);
        return $this;
    }
    public function addHtmlSpecialChars(): StringBuilder
    {
        $this->string = htmlspecialchars($this->string,ENT_QUOTES);
        return $this;
    }
}
