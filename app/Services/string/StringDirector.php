<?php

namespace App\Services\string;

use PhpParser\Builder;

class StringDirector
{
    public string $string;

    public function __construct(string $string)
    {
        $stringBuilder = new StringBuilder($string);
        $this->string = $stringBuilder
            ->findLinks()
            ->findPostReply()
            ->addHtmlSpecialChars()
            ->mdLinkToHtml()
            ->addBr()
            ->tagsToHtml()
            ->build();
    }


}
