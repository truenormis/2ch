<?php

namespace App\Services\sort;

use App\Models\Board;
use Illuminate\Http\Request;

class SortService
{
    private string $sort;
    private bool $direction;

    private array $sortClass =
        [
            'default' => LastPostSort::class,
            'last_thread' => LastPostSort::class,
            'post_count' => CountPostSort::class,
            'hot' => HotSort::class
        ];

    public function __construct(Request $request)
    {
        $key = $request->get('sort');
        $keyInfo = $this->getKeyInfo($key);
        $this->sort = $keyInfo['sort'];
        $this->direction = $keyInfo['direction'];
    }

    private function getKeyInfo($key): array
    {
        if (isset($this->sortClass[$key])) {
            return [
                'sort' => $key,
                'direction' => true
            ];
        } elseif (isset($key[0]) && $key[0] === '-' && isset($this->sortClass[substr($key, 1)])) {

            return [
                'sort' => substr($key, 1),
                'direction' => false
            ];
        } else {
            return [
                'sort' => 'default',
                'direction' => true
            ];
        }
    }

    public function sort(Board $board)
    {
        $sort = $this->sortClass[$this->sort];
        return $sort::sort($board, $this->direction);
    }
}
