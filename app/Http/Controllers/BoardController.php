<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Services\PostCreateService;
use App\Services\sort\SortService;
use App\Services\ThreadCreateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Stevebauman\Location\Facades\Location;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(string $board, SortService $sort)
    {


        $board = Board::where('board', $board)->firstOrFail();
        $threads = $sort->sort($board);
        return view('board.board')->with('pageBoard',$board)->with('threads',$threads);
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
