<?php

namespace App\Http\Requests;

use App\Rules\ExtensionRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class CreateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */

    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'text' => 'required|max:1500|min:5',
            'sage' => 'boolean',
            'op' => 'boolean',
            'file' => ['max:3',new ExtensionRule()],
            'file*' => File::types(['mp4','webm','webp','jpg','jpeg','png'])->min('1kb')->max('10mb'),
        ];
    }
}
