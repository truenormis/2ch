<?php

namespace App\Services\KeyChecker;

use App\Models\Thread;

use App\Services\UserKeyGeneratorService;
use Illuminate\Database\Eloquent\Model;

class ThreadKeyChecker
{


    public static function check(Thread $thread): bool
    {
        $gen = app(UserKeyGeneratorService::class);
        return $gen->check($thread->op_key);
    }
}
