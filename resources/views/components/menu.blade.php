@php $boards = \App\Models\Board::all() @endphp
<div class="text-center md:hidden">
    <button class=" absolute text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800" type="button" data-drawer-target="drawer-navigation" data-drawer-show="drawer-navigation" aria-controls="drawer-navigation">
        Show navigation
    </button>
</div>
<div class="hidden md:w-1/4 md:block bg-white p-5 rounded-xl shadow">
{{--    <div class="w-full bg-gray-200 p-2 rounded-t-lg shadow-md">--}}
{{--        Разное--}}
{{--    </div>--}}
{{--    <ul class="p-3 max-w-md space-y-1 text-gray-500 list-none list-inside dark:text-gray-400">--}}
{{--        @foreach($boards as $board)--}}
{{--            <li>--}}
{{--                <a class="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="{{route('board',['board'=>$board->board])}}">/{{$board->board}}/ - {{$board->name}}</a>--}}
{{--            </li>--}}
{{--        @endforeach--}}
{{--    </ul>--}}
    <h5 id="drawer-navigation-label" class="text-base font-semibold text-gray-500 uppercase dark:text-gray-400">Разное</h5>
    <button type="button" data-drawer-hide="drawer-navigation" aria-controls="drawer-navigation" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 end-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" >
        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        <span class="sr-only">Close menu</span>
    </button>
    <div class="py-4 overflow-y-auto">
        <ul class="space-y-2 font-medium">
            @foreach($boards as $board)
                {{--                <li>--}}
                {{--                    <a class="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="{{route('board',['board'=>$board->board])}}">/{{$board->board}}/ - {{$board->name}}</a>--}}
                {{--                </li>--}}
                <li>

                    <a href="{{route('board',['board'=>$board->board])}}" class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                        <span class="ms-3">/{{$board->board}}/ - {{$board->name}}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>


<!-- drawer init and show -->


<!-- drawer component -->
<div id="drawer-navigation" class="md:hidden fixed top-0 left-0 z-40 w-64 h-screen p-4 overflow-y-auto transition-transform -translate-x-full bg-white dark:bg-gray-800" tabindex="-1" aria-labelledby="drawer-navigation-label">
    <h5 id="drawer-navigation-label" class="text-base font-semibold text-gray-500 uppercase dark:text-gray-400">Разное</h5>
    <button type="button" data-drawer-hide="drawer-navigation" aria-controls="drawer-navigation" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 absolute top-2.5 end-2.5 inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" >
        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        <span class="sr-only">Close menu</span>
    </button>
    <div class="py-4 overflow-y-auto">
        <ul class="space-y-2 font-medium">
            @foreach($boards as $board)
{{--                <li>--}}
{{--                    <a class="font-medium text-blue-600 dark:text-blue-500 hover:underline" href="{{route('board',['board'=>$board->board])}}">/{{$board->board}}/ - {{$board->name}}</a>--}}
{{--                </li>--}}
                <li>

                    <a href="{{route('board',['board'=>$board->board])}}" class="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 group">
                        <span class="ms-3">/{{$board->board}}/ - {{$board->name}}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
