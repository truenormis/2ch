<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class post extends Component
{
    public bool $idstat;

    /**
     * Create a new component instance.
     * @param post $post
     * @param bool $id_status $
     */
    public function __construct(public Post $post,  $idstat)
    {
        $this->idstat = $idstat;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.post');
    }
}
