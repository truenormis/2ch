<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Database Table</title>
    @vite(['resources/css/app.css','resources/js/app.js'])
</head>
<body class="bg-gray-100">

<div class="text-center m-20">
    <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">{{$pageBoard->name}}</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <x-modal.btn></x-modal.btn>
    <x-modal.modal :board="$pageBoard"></x-modal.modal>
</div>

<div class="container mx-auto flex  ">
    <x-menu></x-menu>
    <div class="w-full p-2 pt-0 flex flex-col">
        @php
        @endphp
        @foreach($threads as $thread)
            <div class="mb-5 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 w-full p-6">
                <x-post :post="$thread->threadPost" idstat="0"></x-post>
                @foreach($thread->recentPosts() as $post)
                    @if($post->thread_post == true)
                        @continue
                    @endif
                @php $owner = $post->check() ? "border-amber-600" : "border-gray-200 " @endphp
                <div class="mt-2 block p-6 bg-white border {{$owner}} rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 w-full">
                        <x-post :post="$post" idstat="0"></x-post>
                </div>
                @endforeach
            </div>
        @endforeach
    </div>


</div>
</body>
</html>
