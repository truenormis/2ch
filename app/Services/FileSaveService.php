<?php

namespace App\Services;

use App\Http\Requests\CreateFormRequest;
use App\Models\File;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Drivers\Gd\Driver;
use Intervention\Image\ImageManager;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class FileSaveService
{
    public function __construct(private CreateFormRequest $request)
    {
    }
    public function save(Post $post): void
    {
        $request = $this->request;

        if ($request->hasFile('file')){
            $files = $request->file('file');
            foreach ($files as $file){

                $name = $file->hashName();

                $upload = Storage::disk('public')->put("media/", $file);
                $thumb = "";
                $manager = new ImageManager(new Driver());
                if (str_contains($file->getClientMimeType(), 'image')) {
                    $thumb = 'media/thumb_'.$name;

                    $image = $manager->read('storage/media/'.$name);
                    $image->scale(height: 200);
                    $image->save("storage/$thumb");

                } elseif (str_contains($file->getClientMimeType(), 'video')) {
                    $thumb = "media/thumb_$name.png";
                    FFMpeg::fromDisk('public')
                        ->open("media/$name")
                        ->getFrameFromSeconds(1)
                        ->addFilter([
                        ])
                        ->export()
                        ->toDisk('public')
                        ->save($thumb);

                    $image = $manager->read("storage/".$thumb);
                    $image->scale(height: 200);
                    $image->save("storage/".$thumb);

                }
                $newFile = File::create([
                    'file_name' => $file->getClientOriginalName(),
                    'mime_type' => $file->getClientMimeType(),
                    'path' => "media/{$name}",
                    'thumb' => $thumb,
                    'post_id'=>$post->id
                ]);

            }

        }
    }
}
