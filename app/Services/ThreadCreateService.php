<?php

namespace App\Services;

use App\Models\Board;
use App\Models\Post;
use App\Models\Thread;

class ThreadCreateService
{
    public function __construct(private readonly UserKeyGeneratorService $generatorService){}

    public function create(Board $board)
    {
        return Thread::create([
           'op_key' => $this->generatorService->gen(),
            'board_id' => $board->id
        ]);
    }
}
