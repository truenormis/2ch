<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\Post;
use App\Models\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_create_thread_route(): void
    {
        $board = Board::factory()->create([
            'board' => 'b',
            'name' => 'name',
            'description' => 'desk'
        ]);

        $response = $this->post(
            route('thread.create', ['board' => $board->board]),
            [
                'text' => 'Hello'
            ]
        );
        $response->assertStatus(200);

    }

    public function test_create_thread(): void
    {
        $board = Board::factory()->create([
            'board' => 'b',
            'name' => 'name',
            'description' => 'desk'
        ]);

        $response = $this->post(
            route('thread.create', ['board' => $board->board]),
            [
                'text' => 'Hello'
            ]
        );
        $this->assertDatabaseCount('threads', 1);
        $this->assertDatabaseCount('posts', 1);
        $this->assertDatabaseHas('posts',
            [
                'text' => 'Hello',
                'Op' => true,
                'Sage' => false
            ]
        );
    }
    public function test_thread_validation_text(): void
    {
        $board = Board::factory()->create([
            'board' => 'b',
            'name' => 'name',
            'description' => 'desk'
        ]);

        $response = $this->post(
            route('thread.create', ['board' => $board->board]),
            [
                'text' => Str::random(1501)
            ]
        );
        $this->assertDatabaseCount('threads', 0);
        $this->assertDatabaseCount('posts', 0);
    }
}
