
@php
    $data = \Carbon\Carbon::parse($post->created_at)->format('d/m/y D H:i:s');
    $sage = $post->sage == 1 ? '<b class="text-red-800 ml-1" >SAGE</b>' : "";
    $op = $post->op == 1 ? '<b class="text-green-400 ml-1" >#OP</b>' : "";
@endphp
<p class="text-gray-500 flex" @if($idstat) id="{{$post->id}}" @endif>
    @php        //$location = Stevebauman\Location\Facades\Location::get('188.163.21.103');
    @endphp
    <x-icon name="flag-country-{{$post->country_code}}" class="w-5 h-6 mr-3"/>
    Аноним {{$data}}
    №<a href="{{route('thread',['board' => $post->thread->board->board,'thread' => $post->thread->id])}}#{{$post->id}}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">{{$post->id}}</a>
    {!! $sage !!}
    {!! $op !!}
</p>
@if(count($post->files) === 1)
    <div class="md:flex">
        <x-image :file="$post->files[0]"></x-image>
        <p class="font-normal text-xl md:pt-10">{!! $post->text !!}</p>
    </div>
@else
    <div class="flex row flex-wrap">
    @foreach($post->files as $file)

        <x-image :file="$file"></x-image>
    @endforeach
    </div>
    <p class="">{!! $post->text !!}</p>
@endif



