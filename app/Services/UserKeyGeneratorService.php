<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserKeyGeneratorService
{
    public function __construct(private readonly Request $request)
    {
    }

    public function gen(): string
    {
        return $this->request->ip().$this->request->server('HTTP_USER_AGENT');
    }
    public function check(string $key):bool{
        return $this->request->ip().$this->request->server('HTTP_USER_AGENT') == $key;
    }
}
