<?php

namespace App\Services;

use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;

class CountryCodeService
{
    private string $ip;
    public function __construct(Request $request)
    {
        $this->ip = $request->ip();
    }

    public function code(): string
    {
        $location = Location::get();
        return \Str::lower($location->countryCode);
    }
}
