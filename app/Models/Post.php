<?php

namespace App\Models;

use App\Services\UserKeyGeneratorService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $text
 * @property int $Sage
 * @property int $Op
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereOp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 * @property int $sage
 * @property int $op
 * @property int $thread_id
 * @property int $thread_post
 * @property-read \App\Models\Thread|null $thread
 * @method static \Database\Factories\PostFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereThreadPost($value)
 * @property int $post_key
 * @method static \Illuminate\Database\Eloquent\Builder|Post wherePostKey($value)
 * @mixin \Eloquent
 */
class Post extends Model
{
    use HasFactory;
    protected $guarded = false;

    public function thread(): BelongsTo
    {
        return $this->belongsTo(Thread::class);
    }
    public function files(): HasMany
    {
        return $this->hasMany(File::class);
    }

    public function check():bool{
        $gen = app(UserKeyGeneratorService::class);
        return $gen->check($this->post_key);
    }
}
