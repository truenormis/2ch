<?php

namespace App\Services\sort;

use App\Models\Board;
use Ramsey\Collection\Collection;

interface ThreadSort
{
    public static function sort(Board $board, bool $direction);
}
