<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<style>

</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{$route}}" enctype="multipart/form-data">
    @csrf
    <div class="w-full mb-4 border border-gray-200 rounded-lg bg-gray-50 dark:bg-gray-700 dark:border-gray-600">
        <div class="px-4 py-2 bg-white rounded-t-lg dark:bg-gray-800">
            <label for="comment" class="sr-only">Your comment</label>
            <textarea id="comment" name="text" rows="4" class="w-full px-0 text-sm text-gray-900 bg-white border-0 dark:bg-gray-800 focus:ring-0 dark:text-white dark:placeholder-gray-400" placeholder="Write a comment..." required></textarea>
        </div>
        <div class="flex flex-col-reverse items-center justify-center md:flex-row md:justify-between  text-ce  px-3 py-2 border-t dark:border-gray-600">
            <button type="submit" class="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white bg-blue-700 rounded-lg focus:ring-4 focus:ring-blue-200 dark:focus:ring-blue-900 hover:bg-blue-800">
                Post
            </button>
            <div class="flex">
                <div class="flex items-center me-4 m-3">
                    <input id="inline-checkbox" name="sage" type="checkbox" value="1" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="inline-checkbox"  class="ms-2 text-sm font-medium text-red-800 dark:text-gray-300">SAGE</label>
                </div>
                <div class="flex items-center me-4">
                    <input id="inline-2-checkbox" name="op" type="checkbox" value="1" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="inline-2-checkbox" class="ms-2 text-sm font-medium text-green-600 dark:text-gray-300">OP</label>
                </div>
            </div>

            <div class="flex ps-0 space-x-1 rtl:space-x-reverse sm:ps-2">
                <x-form.icon id="format_bold"></x-form.icon>
                <x-form.icon id="format_italic"></x-form.icon>
                <x-form.icon id="format_quote"></x-form.icon>
                <x-form.icon id="format_underlined"></x-form.icon>
                <x-form.icon id="format_overline"></x-form.icon>
                <x-form.icon id="strikethrough_s" ></x-form.icon>
            </div>
        </div>
        <input type="file" name="file[]" multiple>
    </div>
</form>
