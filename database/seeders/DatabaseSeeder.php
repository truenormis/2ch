<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Board;
use App\Models\Post;
use App\Models\Thread;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Board::factory(2)->sequence(
            ['board' => 'b','name'=>'Бред','description'=>'бред'],
            ['board' => 'po','name'=>'Политика','description'=>'политика, новости, ольгинцы, хохлы, либерахи, рептилоиды.. oh shi'],
        )
            ->has(
                Thread::factory()->count(10)
                    ->has(
                        Post::factory(1)
                            ->sequence(['thread_post' => true,'op'=>true,'sage'=>false])

                    )->has(Post::factory(50))
            )
            ->create();

    }
}
