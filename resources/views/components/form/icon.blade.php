<button type="button" class="inline-flex justify-center items-center p-1 text-gray-500 rounded cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
    <span class="material-symbols-rounded">
    {{$id}}
    </span>
</button>
