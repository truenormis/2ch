<?php

namespace App\Listeners;

use App\Events\BumpLimit;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CloseThread
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(BumpLimit $event): void
    {
        $thread = $event->thread;
        $board_limit= $thread->board->bump_limit;
        $thread->update(['close'=>true]);
    }
}
