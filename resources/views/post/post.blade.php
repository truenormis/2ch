<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Database Table</title>
    @vite(['resources/css/app.css','resources/js/app.js'])
</head>
<body class="bg-gray-100">

<div class="text-center m-5 md:m-20">
    <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">{{$pageBoard->name}}</h1>
    <div class="w-full flex flex-col items-center">

        @if($thread->close)
            <h3 class="text-3xl font-bold dark:text-white">Тред в архиве</h3>

        @else
        <div class="w-full lg:w-2/4 mt-10">

            <x-form :route="route('post.create',['board'=>$thread->board->board,'thread'=>$thread->id])"></x-form>
        </div>
        @endif
    </div>
</div>
<style>
    /* styles.css */
    @keyframes changeColor {
        from {
            background-color: darkorange; /* конечный цвет фона */

        }
        to {
            background-color: #ffffff; /* начальный цвет фона */
        }
    }

    /* Подключение анимации к классу */
    .bg-color-transition {
        animation: changeColor 6s ease; /* настройка анимации: название, длительность, тип сглаживания */
    }

</style>
<div class="container mx-auto flex  ">
    <x-menu></x-menu>
    <div class="w-full p-2 pt-0 flex flex-col">

            <div class="mb-5 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 w-full p-6">
                <x-post :post="$thread->threadPost" :idstat="0"></x-post>
                @foreach($thread->posts as $post)
                    @if($post->thread_post == true)
                        @continue
                    @endif
                        @php $owner = $post->check() ? "border-amber-600" : "border-gray-200 " @endphp

                        <div class="mt-2 block p-6  border {{$owner}} rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 w-full ">
                        <x-post :post="$post" idstat="1"></x-post>
                </div>
                @endforeach
            </div>

    </div>

    <script>
        const hash = window.location.hash;
        if (hash) {
            const targetId = hash.substring(1);
            const targetElement = document.getElementById(targetId);
            if (targetElement) {
                targetElement.scrollIntoView();
                const parentElement = targetElement.parentElement;
                if (parentElement) {
                    parentElement.classList.add("bg-color-transition");
                }
            }
        }

    </script>
</div>
<p class=" text-orange-300">dsdsds</p>
</body>
</html>
