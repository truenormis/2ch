<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFormRequest;
use App\Models\Board;
use App\Models\Thread;
use App\Services\FileSaveService;
use App\Services\PostCreateService;
use App\Services\ThreadCreateService;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    public function __construct(private readonly ThreadCreateService $thread)
    {
    }
    public function index(string $board, Thread $thread){
        $board = Board::where('board', $board)->firstOrFail();
        if ($thread->board != $board){
            abort(404,'Тред не найден');
        }
        return view('post.post')->with('pageBoard',$board)->with('thread',$thread);
    }

    public function create(string $board, CreateFormRequest $request,FileSaveService $fileSaveService)
    {
        //$file = $request->file('file')[0]->getClientMimeType();
        //dd($file);
        $validated = $request->safe()->only(['text']);
        $board = Board::where('board', $board)->firstOrFail();
        if(count($request->file()) == 0){
            return redirect()->back()->withErrors('Для создания треда необходимо загрузить медиа');
        }
        $thread = $this->thread->create($board);
        $post = PostCreateService::create($thread,$validated['text'],true,false,true);
        $fileSaveService->save($post);
        return redirect()->route('thread',['board'=>$board->board,'thread'=>$thread]);
    }
}
