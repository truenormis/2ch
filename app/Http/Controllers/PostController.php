<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFormRequest;
use App\Models\File;
use App\Models\Thread;
use App\Services\FileSaveService;
use App\Services\PostCreateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Gd\Driver;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class PostController extends Controller
{
    public function __construct(private FileSaveService $fileSaveService)
    {

    }

    public function create(string $board,Thread $thread,CreateFormRequest $request)
    {

        if ($thread->close){
            abort(403, 'Тред закрыт. Нельзя создать новый пост.');
        }
        $validated = $request->validated();

        $op = isset($validated['op']);
        $sage = isset($validated['sage']);
        $post = PostCreateService::create($thread,$validated['text'],$op,$sage);
        $this->fileSaveService->save($post);



        return redirect()->back();
    }
}
