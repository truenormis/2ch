<?php

namespace App\Http\Controllers;

use App\Models\Board;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $boards = Board::all();
        return view('home.home')->with('boards',$boards);
    }
}
