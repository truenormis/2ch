@extends('errors::minimal')

@section('title', "Ничего не найдено")
@section('code', '404')
@section('message', "Ничего не найдено")
