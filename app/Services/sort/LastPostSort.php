<?php

namespace App\Services\sort;

use App\Models\Board;
use App\Models\Thread;
use Ramsey\Collection\Collection;

class LastPostSort implements ThreadSort
{

    public static function sort(Board $board, bool $direction): array|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_Thread_C
    {
        $directionSTR = $direction ? "DESC" : "ASC";
        return Thread::with('posts')
            ->select(['threads.id','threads.op_key'])
            ->leftJoin('posts', 'threads.id', '=', 'posts.thread_id')
            ->where('threads.board_id', '=', $board->id)
            ->where('threads.close','=',0)
            ->groupBy('threads.id', 'threads.op_key','threads.board_id','threads.op_key')
            ->orderByRaw("MAX(posts.created_at) $directionSTR")
            ->get();
    }
}
