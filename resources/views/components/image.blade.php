
<div class="p-2">
    <a class="text-sm" data-popover-target="popover-filename-{{$file->id}}" >{{Str::limit($file->file_name,15)}}</a>
    <p class="text-sm">107Кб 761х807</p>
    <img src="{{asset("storage/{$file->thumb}")}}" alt="">
</div>

<div data-popover id="popover-filename-{{$file->id}}" role="tooltip" class="absolute z-10 invisible inline-block w-64 text-sm transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm opacity-0 dark:text-gray-400 dark:border-gray-600 dark:bg-gray-800">

    <div class="px-3 py-2 text-gray-800 break-words">
        <p>{{$file->file_name}}</p>
    </div>
    <div data-popper-arrow></div>
</div>
