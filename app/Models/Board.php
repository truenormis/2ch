<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Board
 *
 * @property int $id
 * @property string $board
 * @property string $name
 * @property string $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Board newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Board newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Board query()
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereBoard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Board whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Thread> $threads
 * @property-read int|null $threads_count
 * @method static \Database\Factories\BoardFactory factory($count = null, $state = [])
 * @mixin \Eloquent
 */
class Board extends Model
{
    use HasFactory;

    public function threads(): HasMany
    {
        return $this->hasMany(Thread::class);
    }

    public function threadsByLastPost(): array|\Illuminate\Database\Eloquent\Collection|\LaravelIdea\Helper\App\Models\_IH_Thread_C
    {
        return Thread::with('posts')
            ->select(['threads.id','threads.op_key'])
            ->leftJoin('posts', 'threads.id', '=', 'posts.thread_id')
            ->where('threads.board_id', '=', $this->id)
            ->where('threads.close','=',0)
            ->groupBy('threads.id', 'threads.op_key','threads.board_id','threads.op_key')
            ->orderByRaw('MAX(posts.created_at) DESC')
            ->get();
    }
}
