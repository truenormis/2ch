<?php

namespace App\Services;

use App\Events\BumpLimit;
use App\Models\Post;
use App\Models\Thread;
use App\Services\string\StringDirector;
use App\View\Components\image;
use phpDocumentor\Reflection\Types\Boolean;

class PostCreateService
{

    public static function create(Thread $thread,string $text,bool $op, bool $sage,bool $post = false){
        $op = $op == true && $thread->check();
        $gen = app(UserKeyGeneratorService::class);
        $post = Post::create([
            'text' => self::makeText($text),
            'Sage' => $sage,
            'Op' => $op,
            'post_key' => $gen->gen(),
            'thread_id'=>$thread->id,
            'thread_post' => $post,
            'country_code' => (app(CountryCodeService::class))->code(),
        ]);
        self::bumpLimitCheck($thread);
        return $post;
    }

    private static function bumpLimitCheck(Thread $thread): void
    {
        if(count($thread->posts) >= $thread->board->bump_limit){
            event(new BumpLimit($thread));
        }
    }
    private static function makeText(string $text): string
    {
        $director = new StringDirector($text);
        return $director->string;
    }

}
